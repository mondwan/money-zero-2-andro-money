"""Main entry of this application."""

import sys

from typing import List
from pathlib import Path
from .money_zero_lib import MoneyZeroDbReader
from .andro_money_lib import AndroMoneyTransactionFactory, AndroMoneyCsvWriter


class MainCli:
    """Cli application."""

    def __init__(self, argv: List[str]):
        """Initialize the cli application."""
        self.argv = argv
        self.db_path_str = argv[1]
        self.db_path = Path(self.db_path_str)
        self.factory = AndroMoneyTransactionFactory()
        has_file = self.db_path.exists()
        if not has_file:
            raise Exception(f"No db file in |{self.db_path}|")

        self.csv_writer = AndroMoneyCsvWriter("./andro-money-output.csv")

    def run(self):
        """Run the application."""
        db_reader = MoneyZeroDbReader(self.db_path)
        try:
            money_zero_transactions = db_reader.get_uk_transactions()
            andro_money_transactions = self.factory.create_from_money_zero_transactions(
                money_zero_transactions
            )
            self.csv_writer.write_csv_file(andro_money_transactions)
        finally:
            db_reader.close_db()


if __name__ == "__main__":
    cli = MainCli(sys.argv)
    cli.run()
