#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Constants of this module."""

MONEY_ZERO_ASSET_ITEM_NOTE_2_ANDRO_MONEY_ACCOUNT = {
    "coinbase": "Coinbase",
    "Morrison": "Morrison Gift card",
    "free trade": "Free Trade",
    "英鎊monzo": "Monzo",
    "英鎊lloyds": "Lloyds",
    "英鎊現金": "GBP Cash",
    "Oyster card": "Oyster card",
}
MONEY_ZERO_INCOME_ITEM_NOTE_2_ANDRO_MONEY_CAT_SUB_CAT = {
    "活動收入": ("Income", "Event"),
    "英鎊增值": ("Income", "Top Up"),
    "現金回贈": ("Income", "Cash Back"),
    "薪資收入": ("Income", "Salary"),
    "償還款項": ("Income", "Repayment"),
}
MONEY_ZERO_EXPENSE_ITEM_NOTE_2_ANDRO_MONEY_CAT_SUB_CAT = {
    "菸酒": ("Food", "Wine"),
    "汽車油費": ("Car/Motor", "Gasoline"),
    "旅遊度假": ("Entertainment", "Travel"),
    "戶外郊遊": ("Entertainment", "Travel"),
    "英鎊網絡費": ("Living", "Internet"),
    "ISA": ("Investment", "ISA"),
    "英鎊電話費": ("Living", "Mobile"),
    "房屋租金": ("Living", "Rent"),
    "英鎊家用": ("Living", "Household"),
    "英鎊午餐": ("Food", "Lunch"),
    "聯誼聚餐": ("Social", "Meal"),
    "英鎊晚餐": ("Food", "Dinner"),
    "電影觀賞": ("Entertainment", "Movie"),
    "住屋傢飾": ("Living", "Furniture"),
    "玩具": ("Entertainment", "Toys"),
    "早餐": ("Living", "Breakfast"),
    "食品雜貨": ("Food", "Grocery Shopping"),
    "送禮請客": ("Social", "Gifts"),
    "飲料": ("Food", "Drink"),
    "證件費用": ("Fee", "Passport"),
    "電話費": ("Living", "Mobile"),
    "捷運搭乘": ("Transportation", "Subway"),
    "網路費": ("Living", "Internet"),
    "午餐": ("Food", "Lunch"),
    "晚餐": ("Food", "Dinner"),
    "公車搭乘": ("Transportation", "Bus"),
}
