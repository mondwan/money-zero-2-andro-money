"""Module for andro Money."""

from dataclasses import dataclass
from pathlib import Path
from .money_zero_lib import MoneyZeroTransaction
from typing import List, Tuple
from .constants import (
    MONEY_ZERO_ASSET_ITEM_NOTE_2_ANDRO_MONEY_ACCOUNT,
    MONEY_ZERO_INCOME_ITEM_NOTE_2_ANDRO_MONEY_CAT_SUB_CAT,
    MONEY_ZERO_EXPENSE_ITEM_NOTE_2_ANDRO_MONEY_CAT_SUB_CAT,
)


@dataclass
class AndroMoneyTransaction:
    """Keep data for AndroMoneyTransaction."""

    amount: float
    category: str
    sub_category: str
    yyyymmdd: str
    from_account: str
    to_account: str
    note: str


class AndroMoneyTransactionFactory:
    """Convert MoneyZeroTransaction to AndroMoneyTransaction."""

    CAT_GENERAL_TRANSFER = ("Transfer", "General Transfer")

    def _is_asset_transfer_transaction(
        self, money_zero_transaction: MoneyZeroTransaction
    ):
        item_notes = [
            money_zero_transaction.from_item_note,
            money_zero_transaction.to_item_note,
        ]
        try:
            for item_note in item_notes:
                self._translate_asset_name_or_raise(item_note)
        except KeyError:
            return False
        return True

    def _get_transaction_cat_and_sub_cat(
        self, money_zero_transaction: MoneyZeroTransaction
    ) -> Tuple[str, str]:
        is_asset_transfer = self._is_asset_transfer_transaction(money_zero_transaction)
        if is_asset_transfer:
            return self.CAT_GENERAL_TRANSFER

        from_item_note = money_zero_transaction.from_item_note
        to_item_note = money_zero_transaction.to_item_note

        cat_sub_cat_1 = MONEY_ZERO_INCOME_ITEM_NOTE_2_ANDRO_MONEY_CAT_SUB_CAT.get(
            from_item_note
        )
        cat_sub_cat_2 = MONEY_ZERO_EXPENSE_ITEM_NOTE_2_ANDRO_MONEY_CAT_SUB_CAT.get(
            to_item_note
        )
        cat_sub_cat = cat_sub_cat_1 or cat_sub_cat_2
        return cat_sub_cat

    @staticmethod
    def _translate_asset_name_or_raise(money_zero_asset_name):
        return MONEY_ZERO_ASSET_ITEM_NOTE_2_ANDRO_MONEY_ACCOUNT[money_zero_asset_name]

    @staticmethod
    def _translate_asset_name_or_empty(money_zero_asset_name):
        return MONEY_ZERO_ASSET_ITEM_NOTE_2_ANDRO_MONEY_ACCOUNT.get(
            money_zero_asset_name, ""
        )

    def create_from_money_zero_transactions(
        self, money_zero_transactions: List[MoneyZeroTransaction]
    ) -> List[AndroMoneyTransaction]:
        """Create list of AndroMoneyTransactions."""
        transactions = []
        for money_zero_transaction in money_zero_transactions:
            yyyymmdd = money_zero_transaction.data_date.strftime("%Y%m%d")
            amount = money_zero_transaction.amount
            note = money_zero_transaction.data_note
            from_item_note = money_zero_transaction.from_item_note
            to_item_note = money_zero_transaction.to_item_note

            cat, sub_cat = self._get_transaction_cat_and_sub_cat(money_zero_transaction)
            from_account = self._translate_asset_name_or_empty(from_item_note)
            to_account = self._translate_asset_name_or_empty(to_item_note)

            transaction = AndroMoneyTransaction(
                amount, cat, sub_cat, yyyymmdd, from_account, to_account, note
            )
            transactions.append(transaction)

        return transactions


class AndroMoneyCsvWriter:
    """Create the csv file for list of andro money transaction."""

    CSV_HEADER_STRS: List[str] = [
        "Id",
        "Currency",
        "Amount",
        "Category",
        "Sub-Category",
        "Date",
        "Expense(Transfer Out)",
        "Income(Transfer In)",
        "Note",
        "Periodic",
        "Project",
        "Payee/Payer",
        "uid",
        "Time",
    ]

    def __init__(self, csv_file_path: Path):
        """Initialize this instance."""
        self.csv_file_path = csv_file_path

    def write_csv_file(self, andro_money_transactions: List[AndroMoneyTransaction]):
        """Write the csv file."""
        data_rows = [", ".join(self.CSV_HEADER_STRS)]
        with open(self.csv_file_path, "w") as f:
            for transaction in andro_money_transactions:
                data_row = [
                    "",
                    "GBP",
                    str(transaction.amount),
                    transaction.category,
                    transaction.sub_category,
                    transaction.yyyymmdd,
                    transaction.from_account,
                    transaction.to_account,
                    transaction.note,
                    "",
                    "",
                    "",
                    "",
                    "",
                ]
                data_row_str = ", ".join(data_row)
                data_rows.append(data_row_str)

            csv_contents = "\n".join(data_rows)
            f.write(csv_contents)
