"""A module for reading money zero database."""


from pathlib import Path
import sqlite3
from typing import List
from dataclasses import dataclass
from datetime import date


@dataclass
class MoneyZeroTransaction:
    """Keep attributes of MoneyZero transaction."""

    make_no: str
    data_date: date
    from_item_note: str
    to_item_note: str
    amount: float
    data_note: str


class MoneyZeroDbReader:
    """Read Money Zero Datbase."""

    UK_TRANSACTION_SQL = """
SELECT
    make_no,
    data_date,
    max(CASE when OUT_MOUNT > 0 then item_note else null END) as from_item_note,
    max(CASE when IN_MOUNT > 0 then item_note else null END) as to_item_note,
    max(in_mount) as amount,
    data_note
FROM "MYMONEY_DATA"
where (
    make_no in (
        SELECT make_no
        FROM "MYMONEY_DATA"
        where ITEM_NOTE in (
            "coinbase", "Morrison", "free trade", "英鎊monzo", "英鎊lloyds", "英鎊現金", "Oyster card", "英鎊增值"
        )
    )
)
group by make_no
order by make_no desc"""

    def __init__(self, money_zero_db_path: Path):
        """Initialize this instance."""
        self.money_zero_db_path = money_zero_db_path
        self.db_conn = sqlite3.connect(money_zero_db_path)

    def get_uk_transactions(self) -> List[MoneyZeroTransaction]:
        """Get transaction for UK assets."""
        db_cursor = self.db_conn.cursor()
        transactions = []
        for row in db_cursor.execute(self.UK_TRANSACTION_SQL):
            cloned_row = [*row]
            yyyy_mm_dd_strs = cloned_row[1].split("/")
            cloned_row[1] = date(*map(int, yyyy_mm_dd_strs))
            cloned_row[4] = float(row[4])
            transaction = MoneyZeroTransaction(*cloned_row)
            transactions.append(transaction)
        return transactions

    def close_db(self):
        """Close the database."""
        self.db_conn.close()
