# money-zero-2-andro-money

Goals

Transfer the sqlite db from money zero 2 the csv file andro money need for UK related accounting.

@last-modified: 08 AUG 2022

# Usage

python -m money-zero-2-andro-money ./src-examples/20220808_money_track.db

## Sqlite db query

* Get all related item notes (assets) for UK

```
SELECT distinct ITEM_NOTE, ITEM_CLASS
FROM "MYMONEY_DATA"
where (DATA_DATE LIKE "2021/1%") or (DATA_DATE LIKE "2022/%")
order by item_class desc, DATA_DATE desc
```

* Get all transactions for UK assets

```
SELECT Make_no, data_date,
 max(CASE when OUT_MOUNT > 0 then item_note else null END) as from_item_note,
 max(CASE when IN_MOUNT > 0 then item_note else null END) as to_item_note,
 max(in_mount) as amount,
 data_note
FROM "MYMONEY_DATA"
where (make_no in (
SELECT make_no
FROM "MYMONEY_DATA"
where ITEM_NOTE in ("coinbase", "Morrison", "free trade", "英鎊monzo", "英鎊lloyds", "英鎊現金", "Oyster card", "英鎊增值")
))
group by make_no
order by make_no desc
```

## CSV format in andro money

* Format

```
Id,Currency,Amount,Category,Sub-Category,Date,Expense(Transfer Out),Income(Transfer In),Note,Periodic,Project,Payee/Payer,uid,Time
```


* Example

```
Id,Currency,Amount,Category,Sub-Category,Date,Expense(Transfer Out),Income(Transfer In),Note,Periodic,Project,Payee/Payer,uid,Time
4,GBP,0,SYSTEM,INIT_AMOUNT,10100101,,GBP cash,,,,,1,
3,GBP,0,SYSTEM,INIT_AMOUNT,10100101,,Oyster,,,,,2,
2,GBP,0,SYSTEM,INIT_AMOUNT,10100101,,Monzo,,,,,3,
1,GBP,0,SYSTEM,INIT_AMOUNT,10100101,,Lloyd,,,,,40DAE43A4BFD4AE98A36E1D523BF203F,
6,GBP,2,Food,Breakfast,20220807,GBP cash,,,,,,faa5dcb527bf4ea38ba897cd720b5b78,1304
5,GBP,1,Food,Breakfast,20220807,GBP cash,,,,,,ea41d6c263a04fba90055c619293977e,1245
9,GBP,5,Income,Salary,20220808,,Monzo,,,,,9a178eb0b2fc47918250cbee68bc2076,935
8,GBP,10,Transfer,General Transfer,20220808,Monzo,Oyster,,,,,d8fdfc10c7584f119b8365b13cd8d095,935
7,GBP,5,Living,Electricity,20220808,Monzo,,,,,,2b740d68dbf84a05b698cf2051669219,934
```
